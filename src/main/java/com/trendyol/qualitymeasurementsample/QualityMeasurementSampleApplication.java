package com.ha.qa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QualityMeasurementSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(QualityMeasurementSampleApplication.class, args);
	}

}
